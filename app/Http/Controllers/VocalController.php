<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class VocalController extends Controller
{
    public function index()
    {
        return view('vocal');
    }

    public function store()
    {
        request()->validate([
            'vocal' => 'required'
        ]);
        return back()->with('result',request('vocal'));
    }
}
