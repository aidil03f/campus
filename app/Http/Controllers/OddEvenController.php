<?php

namespace App\Http\Controllers;

class OddEvenController extends Controller
{
    public function index()
    {
        return view('odd-even');
    }

    public function store()
    {
        session()->flash('x',request('number1'));
        session()->flash('y',request('number2'));
        return back();
    }
}
