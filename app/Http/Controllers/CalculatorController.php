<?php

namespace App\Http\Controllers;


class CalculatorController extends Controller
{
    public function index()
    {
        return view('calculator');
    }

    public function tambah()
    {
        $result = request('number-1') + request('number-2');
        return back()->with('results',$result);
    }

     public function kurang()
    {
        $result = request('number-1') - request('number-2');
        return back()->with('results',$result);
    }

     public function kali()
    {
        $result = request('number-1') * request('number-2');
        return back()->with('results',$result);
    }

     public function bagi()
    {
        if(request('number-2') <= 0)
        {
            $result = 'maaf, tidak bisa dilakukan';
        }else{
            $result = request('number-1') / request('number-2');
        }
        return back()->with('results',$result);
    }
}
