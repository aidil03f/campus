<?php

use App\Http\Controllers\CalculatorController;
use App\Http\Controllers\OddEvenController;
use App\Http\Controllers\VocalController;
use Illuminate\Support\Facades\Route;

Route::view('/','welcome');
Route::view('/home','home');

Route::group(['prefix' => 'calculator'],function(){
    Route::get('',[CalculatorController::class,'index']);
    Route::post('/tambah',[CalculatorController::class,'tambah']);
    Route::post('/kurang',[CalculatorController::class,'kurang']);
    Route::post('/kali',[CalculatorController::class,'kali']);
    Route::post('/bagi',[CalculatorController::class,'bagi']);
});

Route::get('/oddeven',[OddEvenController::class,'index']);
Route::post('/oddeven/create',[OddEvenController::class,'store']);

Route::get('/vocal',[VocalController::class,'index']);
Route::post('/vocal/create',[VocalController::class,'store']);

