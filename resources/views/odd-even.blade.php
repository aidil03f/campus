@extends('layouts.app',['title' => 'odd & even'])

@section('content')
    <h1>odd & even</h1>
    <form action="/oddeven/create" method="post" autocomplete="off">
        @csrf
        <div class="form-group d-flex">
            <input type="number" name="number1" class="form-control col-3" placeholder="bilangan1" required>
            <input type="number" name="number2" class="form-control col-3" placeholder="bilangan2" required>
            <button type="submit" id="submit" class="btn btn-secondary">Go</button>
        </div>
    </form>
   
    <table class="table">
        <thead class="thead-dark">
            <tr>
                <th>result</th>
            </tr>
        </thead>
        <tbody>
           @for($x = session('x'); $x <= $y = session('y'); $x++)
                <tr>
                    @if($x % 2 == 0)
                        <td>{{ $x ? "angka $x adalah genap" : '' }}</td>
                    @else
                        <td>{{ $x ? "angka $x adalah ganjil" : '' }}</td>
                    @endif
                </tr>
           @endfor
        </tbody>
    </table>
@endsection