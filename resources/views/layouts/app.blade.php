<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <title>{{ $title ?? 'App' }}</title>
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
        <div class="navbar-brand">Apps</div>
        <div class="collapse navbar-collapse">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link{{ request()->is('home') ? ' active' : '' }}" href="/home">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link{{ request()->is('calculator') ? ' active' : '' }}" href="/calculator">Calculator</a>
                </li>
                 <li class="nav-item">
                    <a class="nav-link{{ request()->is('oddeven') ? ' active' : '' }}" href="/oddeven">odd & even</a>
                </li>
                 <li class="nav-item">
                    <a class="nav-link{{ request()->is('vocal') ? ' active' : '' }}" href="/vocal">vocal</a>
                </li>
            </ul>
        </div>
    </nav>
    <script src="{{ asset('js/app.js') }}"></script>
    <div class="container mt-5">
        @yield('content')
    </div>
</body>
</html>