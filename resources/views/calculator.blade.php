@extends('layouts.app')

@section('content')
<div class="row justify-content-center">
    <div class="col-md-4">
        <div class="card">
            <div class="card-header">Calculator</div>
            <div class="card-body">
               <form action="" method="post">
                   @csrf
                   @if(Session::has('results'))
                        <div class="alert alert-success">{{ Session::get('results')}}</div>
                   @endif
                   <div class="form-group">
                     <label>Number 1</label>
                     <input type="number" name="number-1" class="form-control" placeholder="number 1" required>
                   </div>
                    <div class="form-group">
                     <label>Number 2</label>
                     <input type="number" name="number-2" class="form-control" placeholder="number 2" required>
                   </div>
                    <button type="submit" id="tambah" class="btn btn-primary col-2 p-2"> + </button>
                    <button type="submit" id="kurang" class="btn btn-primary col-2 p-2"> - </button>
                    <button type="submit" id="bagi" class="btn btn-primary col-2 p-2"> / </button>
                    <button type="submit" id="kali" class="btn btn-primary col-2 p-2"> x </button>
               </form>
            </div>
        </div>
    </div>
</div>

<script>

    $('#tambah').click(function(){
        $('form').attr('action','/calculator/tambah');    
    });

    $('#kurang').click(function(){
        $('form').attr('action','/calculator/kurang');    
    });

    $('#bagi').click(function(){
        $('form').attr('action','/calculator/bagi');    
    });

    $('#kali').click(function(){
        $('form').attr('action','/calculator/kali');
    });

</script>

@endsection