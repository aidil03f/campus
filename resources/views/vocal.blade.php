@extends('layouts.app',['title'=> 'vocal'])

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">
                    vocal
                </div>
                <div class="card-body">
                    <form action="/vocal/create" method="post">
                        @csrf
                        <div class="form-group">
                            @if($string = session('result'))
                                @php
                                    $a = preg_match('/[a]/',$string);
                                    $e = preg_match('/[e]/',$string);
                                    $i = preg_match('/[i]/',$string);
                                    $o = preg_match('/[o]/',$string);
                                    $u = preg_match('/[u]/',$string);
                                    $result = $a+$e+$i+$o+$u;
                                    if($a + $e == 2){
                                        echo "<p class='alert alert-success'>$string = $result yaitu a dan e</p>";
                                    }elseif($a + $u == 2){
                                        echo "<p class='alert alert-success'>$string = $result yaitu a dan u</p>";
                                    }elseif($i + $u == 2){
                                        echo "<p class='alert alert-success'>$string = $result yaitu i dan u</p>";
                                    }
                                @endphp
                            @endif
                            <textarea name="vocal" class="form-control" cols="20" rows="3"></textarea>
                            @error('vocal')
                                <div class="text-danger mt-2">{{ $message }}</div>
                            @enderror
                        </div>
                        <button type="submit" class="btn btn-primary col-12">submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection